# README SISTEMA 3 DE 3#

El presente proyecto es un prototipo funcional para la final del Reto **#Sistema3de3** y es desarrollado por [Nicotina Estudio](http://www.nicotinaestudio.com). 

##El reto:
La Contraloría del Estado es la dependencia responsable de ejecutar la auditoría de la Administración Pública Central y Paraestatal y de aplicar el derecho disciplinario a los Servidores Públicos.

La Declaración Patrimonial es un documento preventivo, que permite evaluar en forma periódica la evolución patrimonial de los Servidores Públicos, con el objeto de fortalecer la confianza de la ciudadanía asegurando la honradez en la Administración Pública. La finalidad de la misma es vigilar que la evolución del patrimonio de los Servidores Públicos de las Dependencias y Organismos del Poder Ejecutivo del Estado, sea congruente con respecto a sus ingresos lícitos.

De conformidad con lo establecido en el artículo 93 de la Ley de Responsabilidades de los Servidores Públicos del Estado de Jalisco, en su fracción II inciso a) señala que están obligados a presentar su Declaración Patrimonial: En el Poder Ejecutivo, ante la Contraloría del Estado, todos los Servidores Públicos de confianza desde el Gobernador del Estado hasta los jefes de sección, incluyendo lo encargados de almacén y quienes con motivo de sus funciones administren fondos y valores del Estado, así como los considerados en los incisos b) al h) de ese artículo; además los señalados en el Acuerdo de Adición de Obligados, publicado en el Periódico Oficial del Estado el 22 de abril de 1999, en observancia al inciso i) del citado artículo 93.

Conforme a las Leyes Generales expedidas en la materia el universo de obligados aumenta a TODOS los servidores públicos de la Administración Pública del Estado.

##Tecnología producción

- Java EE
- PostgreSQL
- HTML5
- jQuey
- CSS3
- Bootstrap

##Tecnología prototipo

- .NET ASP MVC
- SQLServer
- C#
- HTML5
- jQuey
- CSS3
- Bootstrap

##Instalación / Configuración 
Clonar el repositorio en su computadora, abrir el archivo **Sistema3de3.sln** con Visual Studio Comunity 2015

Para ejecutar de forma local:

- Cambiar la cadena de conexión en el archivo **Web.config** y apuntarlo al servidor local de SQLServer
- Desde Visual Studio ejecutar: Debug -> Start Without Debugging

##Screenshots
![alt tag](http://s3d3.nicotinaestudio.com/Content/Imagenes/Capturas/modulo-1.png)
![alt tag](http://s3d3.nicotinaestudio.com/Content/Imagenes/Capturas/modulo-2.png)
![alt tag](http://s3d3.nicotinaestudio.com/Content/Imagenes/Capturas/modulo-3.png)

##Demo
- [http://s3d3.nicotinaestudio.com](http://s3d3.nicotinaestudio.com)

##Panel de administración
- [http://s3d3.nicotinaestudio.com/Usuario/IniciarSesion](http://s3d3.nicotinaestudio.com/Usuario/IniciarSesion)

Usuario: admin@s3d3.com
Contraseña: admin@s3d3.com

##¿Preguntas o problemas? 
Si usted tiene cualquier duda o pregunta, nos puede contactar por correo <hola@nicotinaestudio.mx>.

##Empresa

**Nuestra Misión**

> *Ofrecemos soluciones tecnológicas que permiten a las empresas digitalizar y mejorar sus procesos institucionales mediante la incorporación de tecnologías modernas, creativas e innovadoras... [Nicotina Estudio](http://www.nicotinaestudio.com)*

##Licencia

Copyright 2017 Nicotina Estudio

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.