/**
* Sistema 3 de 3 para Gobierno del Estado de Jalisco
*
* Desarrollado por Nicotina Estudio
* http://www.nicotinaestudio.com - hola@nicotinaestudio.mx
*
* Creado por: Carlos Isaac Hernández Morfín.
* Fecha de creación: 19/01/2017
**/


using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MujeresEmpoderadas
{
    public class Utilidades
    {
        /// <summary>
        /// Genera un código único.
        /// </summary>
        /// <returns></returns>
        public static string CodigoUnico()
        {
            string characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string ticks = DateTime.UtcNow.Ticks.ToString();
            var code = "";
            for (var i = 0; i < characters.Length; i += 2)
            {
                if ((i + 2) <= ticks.Length)
                {
                    var number = int.Parse(ticks.Substring(i, 2));
                    if (number > characters.Length - 1)
                    {
                        var one = double.Parse(number.ToString().Substring(0, 1));
                        var two = double.Parse(number.ToString().Substring(1, 1));
                        code += characters[Convert.ToInt32(one)];
                        code += characters[Convert.ToInt32(two)];
                    }
                    else
                        code += characters[number];
                }
            }
            return code;
        }

        public static bool esAdmin(string correoElectronico)
        {
            if (correoElectronico == "admin@s3d3.com")
                return true;
            else
                return false;
        }

        #region Seguridad

        public static string encriptarContrasena(string strContrasena)
        {
            string strSal = "";
            string strHash;

            strSal = CrearSal(strContrasena);
            strHash = HashContrasena(strSal, strContrasena);

            using (MD5 md5Hash = MD5.Create())
                strContrasena = ObtenerMd5Hash(md5Hash, strContrasena);

            return strContrasena;
        }

        public static string CrearSal(string strContrasena)
        {
            Rfc2898DeriveBytes hasher = new Rfc2898DeriveBytes(strContrasena,
                System.Text.Encoding.Default.GetBytes("NEMUEM@NicotinaEstudioMX"), 10000);
            return Convert.ToBase64String(hasher.GetBytes(25));
        }

        public static string HashContrasena(string Sal, string Contrasena)
        {
            Rfc2898DeriveBytes Hasher = new Rfc2898DeriveBytes(Contrasena,
                System.Text.Encoding.Default.GetBytes(Sal), 10000);
            return Convert.ToBase64String(Hasher.GetBytes(25));
        }

        static string ObtenerMd5Hash(MD5 md5Hash, string input)
        {

            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        // Verify a hash against a string. 
        static bool VerificarMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input. 
            string hashOfInput = ObtenerMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}
