﻿

using Rotativa;
/**
* Sistema 3 de 3 para Gobierno del Estado de Jalisco
*
* Desarrollado por Nicotina Estudio
* http://www.nicotinaestudio.com - hola@nicotinaestudio.mx
*
* Creado por: Carlos Isaac Hernández Morfín.
* Fecha de creación: 18/01/2017
**/
using Sistema3de3.DAL;
using Sistema3de3.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Sistema3de3.Controllers
{
    public class ControlController : Controller
    {
        private S3Context db = new S3Context();

        public ActionResult Index()
        {
            return View();
        }

        #region MÓDULO 2.1

        public ActionResult Modulo2_1()
        {
            return View();
        }

        public ActionResult Administradores()
        {
            return View(db.Usuarios.ToList());
        }

        public ActionResult AgregarAdministrador()
        {
            ViewBag.EntidadId = new SelectList(db.Entidades, "EntidadId", "Nombre");
            ViewBag.UnidadResponsableId = new SelectList(db.UnidadesResponsables, "UnidadResponsableId", "Nombre");
            ViewBag.CargoId = new SelectList(db.Cargos, "CargoId", "Nombre");

            return View();
        }

        [HttpPost]
        public ActionResult AgregarAdministrador(Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Usuarios.Add(usuario);
                    db.SaveChanges();

                    return RedirectToAction("Administradores");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "Ocurrio un error en el servidor, por favor inténtalo más tarde.");
                    return View();
                }
            }

            return View();
        }

        public ActionResult Reportes()
        {
            return View(db.ServidoresPublicos.ToList());
        }

        public ActionResult Exportar()
        {
            return new ActionAsPdf("GeneratePDF");
        }

        public ActionResult GeneratePDF()
        {
            var data = db.ServidoresPublicos.ToList();
            return View("Reportes", data);
        }

        public ActionResult Catalogos()
        {
            return View();
        }
        #endregion
    }
}