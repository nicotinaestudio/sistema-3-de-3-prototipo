/**
* Sistema 3 de 3 para Gobierno del Estado de Jalisco
*
* Desarrollado por Nicotina Estudio
* http://www.nicotinaestudio.com - hola@nicotinaestudio.mx
*
* Creado por: Carlos Isaac Hernández Morfín.
* Fecha de creación: 18/01/2017
**/

using System.Collections.Generic;
using System.Linq;
using Sistema3de3.Models;
using System.Web.Mvc;
using System.Web.Security;
using Sistema3de3.DAL;
using System.Text;
using MujeresEmpoderadas;

namespace Sistema3de3.Controllers
{
    public class UsuarioController : Controller
    {
        private S3Context db = new S3Context();

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        #region AUTENTICACIÓN

        public ActionResult IniciarSesion()
        {
            return View();
        }

        [HttpPost]
        public ActionResult IniciarSesion([Bind(Include = "CorreoElectronico, Contrasenia")]Login login)
        {
            if (ModelState.IsValid)
            {
                // Validamos si es Admin
                if (Utilidades.esAdmin(login.CorreoElectronico))
                {
                    List<string> session = new List<string>();
                    session.Add("SP");
                    session.Add(login.CorreoElectronico);
                    session.Add(login.CorreoElectronico);
                    session.Add("SP");

                    StringBuilder builder = new StringBuilder();
                    foreach (string var in session)
                    {
                        builder.Append(var).Append("|");
                    }

                    FormsAuthentication.SetAuthCookie(builder.ToString(), false);

                    return Redirect("/Registro");
                }

                var sp = db.ServidoresPublicos.Where(x => x.CorreoElectronico == login.CorreoElectronico
                && x.Contrasena == login.Contrasenia && x.EsActivo == true).FirstOrDefault();

                if (sp == null)
                    ModelState.AddModelError("", "Datos de acceso incorrectos");
                else
                {

                    List<string> session = new List<string>();
                    session.Add("SP");
                    session.Add(login.CorreoElectronico);
                    session.Add(login.CorreoElectronico);
                    session.Add("SP");

                    StringBuilder builder = new StringBuilder();
                    foreach (string var in session)
                    {
                        builder.Append(var).Append("|");
                    }

                    FormsAuthentication.SetAuthCookie(builder.ToString(), false);

                    return Redirect("/Presentacion/Index/" + sp.UsuarioId);
                }
            }
            return View(login);
        }

        [Authorize]
        public ActionResult CerrarSesion()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        #endregion
    }
}