﻿

using Sistema3de3.DAL;
/**
* Sistema 3 de 3 para Gobierno del Estado de Jalisco
*
* Desarrollado por Nicotina Estudio
* http://www.nicotinaestudio.com - hola@nicotinaestudio.mx
*
* Creado por: Carlos Isaac Hernández Morfín.
* Fecha de creación: 18/01/2017
**/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sistema3de3.Controllers
{
    public class PresentacionController : Controller
    {
        private S3Context db = new S3Context();

        public ActionResult Index(int? id)
        {
            var idx = id;

            if(id == null)
                idx = db.ServidoresPublicos.FirstOrDefault().UsuarioId;

            return View(db.ServidoresPublicos.Find(idx));
        }

        public ActionResult DeclaracionesPendientes(int id)
        {
            return View(db.ServidoresPublicos.Find(id));
        }

        public ActionResult EditarDeclaracion(int id)
        {
            return View(db.Declaraciones.Find(id));
        }

        public ActionResult Patrimoniales()
        {
            return View();
        }
    }
}