﻿using Sistema3de3.DAL;
using Sistema3de3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sistema3de3.Controllers
{
    public class RegistroController : Controller
    {
        private S3Context db = new S3Context();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ServidoresPublicos()
        {
            return View(db.ServidoresPublicos.ToList());
        }

        public ActionResult AgregarServidor()
        {
            ViewBag.EntidadId = new SelectList(db.Entidades, "EntidadId", "Nombre");
            ViewBag.UnidadResponsableId = new SelectList(db.UnidadesResponsables, "UnidadResponsableId", "Nombre");
            ViewBag.CargoId = new SelectList(db.Cargos, "CargoId", "Nombre");

            return View();
        }

        [HttpPost]
        public ActionResult AgregarServidor(ServidorPublico usuario)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.ServidoresPublicos.Add(usuario);
                    db.SaveChanges();

                    return RedirectToAction("ServidoresPublicos");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "Ocurrio un error en el servidor, por favor inténtalo más tarde.");
                    return View();
                }
            }

            return View();
        }


        public ActionResult EditarServidorPublico(int id)
        {
            ViewBag.EntidadId = new SelectList(db.Entidades, "EntidadId", "Nombre");
            ViewBag.UnidadResponsableId = new SelectList(db.UnidadesResponsables, "UnidadResponsableId", "Nombre");
            ViewBag.CargoId = new SelectList(db.Cargos, "CargoId", "Nombre");

            return View(db.ServidoresPublicos.Find(id));
        }


        public ActionResult DetalleServidorPublico(int id)
        {
            return View(db.ServidoresPublicos.Find(id));
        }


        public ActionResult AgregarObligacion()
        {
            ViewBag.UsuarioId = new SelectList(db.ServidoresPublicos, "UsuarioId", "RFC");

            return View();
        }

        [HttpPost]
        public ActionResult AgregarObligacion(Declaracion declaracion)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var serv = db.ServidoresPublicos.Find(declaracion.UsuarioId);

                    serv.Declaraciones.Add(declaracion);
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "Ocurrio un error en el servidor, por favor inténtalo más tarde.");
                    return View();
                }
            }

            return View();
        }
    }
}
