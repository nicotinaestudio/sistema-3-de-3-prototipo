/**
* Sistema 3 de 3 para Gobierno del Estado de Jalisco
*
* Desarrollado por Nicotina Estudio
* http://www.nicotinaestudio.com - hola@nicotinaestudio.mx
*
* Creado por: Carlos Isaac Hernández Morfín.
* Fecha de creación: 19/01/2017
**/

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sistema3de3.Models
{
    [Table("Declaraciones")]
    public class Declaracion
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public Declaracion()
        {
            EsActivo = true;
            EsEnviado = false;
            EsEliminado = false;
            FechaDeRegistro = DateTime.Now;
            AcuseUID = "";
        }

        [Key]
        [ScaffoldColumn(false)]
        public int DeclaracionId { get; set; }

        [Required()]
        [ScaffoldColumn(false)]
        public bool EsActivo { get; set; }

        [Required()]
        [ScaffoldColumn(false)]
        public bool EsEnviado { get; set; }
        
        [Required()]
        [ScaffoldColumn(false)]
        public bool EsEliminado { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        public DateTime FechaDeRegistro { get; set; }

        [Display(Name = "Concepto de la declaración")]
        public DeclaracionConcepto DeclaracionConcepto { get; set; }

        [Display(Name = "Tipo de declaración")]
        public DeclaracionTipo DeclaracionTipo { get; set;}

        [Display(Name = "Acuse de recepción")]
        public string AcuseUID { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Campo 1")]
        public string Campo1 { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Campo 2")]
        public string Campo2 { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Campo 3")]
        public string Campo3 { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Campo 4")]
        public string Campo4 { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Campo 5")]
        public string Campo5 { get; set; }

        public int UsuarioId { get; set; }
        public virtual ServidorPublico ServidorPublico { get; set; }
    }
}