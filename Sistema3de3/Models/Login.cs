﻿/**
* Sistema 3 de 3 para Gobierno del Estado de Jalisco
*
* Desarrollado por Nicotina Estudio
* http://www.nicotinaestudio.com - hola@nicotinaestudio.mx
*
* Creado por: Carlos Isaac Hernández Morfín.
* Fecha de creación: 18/01/2017
**/

using System.ComponentModel.DataAnnotations;

namespace Sistema3de3.Models
{
    public class Login
    {
        [DataType(DataType.EmailAddress)]
        [StringLength(128)]
        [Required()]
        [Display(Name = "Correo electrónico")]
        public string CorreoElectronico { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        [Required()]
        public string Contrasenia { get; set; }
    }
}