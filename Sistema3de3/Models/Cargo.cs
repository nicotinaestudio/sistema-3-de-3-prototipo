﻿/**
* Sistema 3 de 3 para Gobierno del Estado de Jalisco
*
* Desarrollado por Nicotina Estudio
* http://www.nicotinaestudio.com - hola@nicotinaestudio.mx
*
* Creado por: Carlos Isaac Hernández Morfín.
* Fecha de creación: 18/01/2017
**/


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sistema3de3.Models
{
    [Table("Cargos")]
    public class Cargo
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public Cargo()
        {
            EsActivo = true;
        }

        [Key]
        [ScaffoldColumn(false)]
        public int CargoId { get; set; }

        [Required()]
        [ScaffoldColumn(false)]
        public bool EsActivo { get; set; }

        [Required()]
        [DataType(DataType.Text)]
        [StringLength(50)]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        public int EntidadId { get; set; }
        public virtual Entidad Entidad { get; set; }
    }
}