﻿/**
* Sistema 3 de 3 para Gobierno del Estado de Jalisco
*
* Desarrollado por Nicotina Estudio
* http://www.nicotinaestudio.com - hola@nicotinaestudio.mx
*
* Creado por: Carlos Isaac Hernández Morfín.
* Fecha de creación: 18/01/2017
**/

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sistema3de3.Models
{
    [Table("Usuarios")]
    public class Usuario
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public Usuario()
        {
            EsActivo = true;
            EsEliminado = false;
            FechaDeRegistro = DateTime.Now;
        }

        [Key]
        [ScaffoldColumn(false)]
        public int UsuarioId { get; set; }

        [Required()]
        [ScaffoldColumn(false)]
        public bool EsActivo { get; set; }

        [Required()]
        [ScaffoldColumn(false)]
        public bool EsEliminado { get; set; }

        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        public DateTime FechaDeRegistro { get; set; }

        [Display(Name = "Nivel de autorización")]
        public NivelUsuario NivelUsuario { get; set; }

        [Required()]
        [DataType(DataType.Text)]
        [StringLength(50)]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Required()]
        [DataType(DataType.Text)]
        [StringLength(50)]
        [Display(Name = "Primer apellido")]
        public string PrimerApellido { get; set; }

        [Required()]
        [DataType(DataType.Text)]
        [StringLength(50)]
        [Display(Name = "Segundo apellido")]
        public string SegundoApellido { get; set; }

        public int EntidadId { get; set; }
        public virtual Entidad Entidad { get; set; }

        public int UnidadResponsableId { get; set; }
        public virtual UnidadResponsable UnidadResponsable { get; set; }

        public int CargoId { get; set; }
        public virtual Cargo Cargo { get; set; }

        [DataType(DataType.Text)]
        [StringLength(15)]
        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Extensión")]
        public string Extension { get; set; }

        [Required()]
        [DataType(DataType.Text)]
        [StringLength(100)]
        [Display(Name = "Correo electrónico")]
        public string CorreoElectronico { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Calle")]
        public string Calle { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Colonia")]
        public string Colonia { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "numero")]
        public string Numero { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Estado")]
        public string Estado { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Municipio")]
        public string Municipio { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "C.P.")]
        public string CP { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Número exterior")]
        public string NumExterior { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Número interior")]
        public string NumInterior { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha de nacimiento")]
        public DateTime FechaDeNacimiento { get; set; }

        [Display(Name = "Estado civil")]
        public EstadoCivil EstadoCivil { get; set; }

        [Required()]
        [DataType(DataType.Text)]
        [StringLength(100)]
        [Display(Name = "Nombre de usuario")]
        public string User { get; set; }

        [Required()]
        [DataType(DataType.Password)]
        [StringLength(50)]
        [Display(Name = "Contraseña")]
        public string Contrasena { get; set; }
    }
}