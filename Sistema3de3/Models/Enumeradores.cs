﻿/**
* Sistema 3 de 3 para Gobierno del Estado de Jalisco
*
* Desarrollado por Nicotina Estudio
* http://www.nicotinaestudio.com - hola@nicotinaestudio.mx
*
* Creado por: Carlos Isaac Hernández Morfín.
* Fecha de creación: 18/01/2017
**/


using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sistema3de3.Models
{
    public enum EstadoCivil
    {
        [Display(Name = "Soltero")]
        Soltero,
        [Display(Name = "Casado")]
        Casado,
        [Display(Name = "Viudo")]
        Viudo
    };

    public enum DeclaracionTipo
    {
        [Display(Name = "Patrimonial")]
        Patrimonial,
        [Display(Name = "Posible conflicto")]
        Conflicto
    };

    public enum DeclaracionConcepto
    {
        [Display(Name = "Inicial")]
        Inicial,
        [Display(Name = "Modificación")]
        Modificacion,
        [Display(Name = "Conclusión del encargo")]
        Conclusion,
    };

    public enum NivelUsuario
    {
        [Display(Name = "Nivel de autorización 1")]
        AutNivel1,
        [Display(Name = "Nivel de autorización 2")]
        AutNivel2,
        [Display(Name = "Nivel de autorización 3")]
        AutNivel3,
        [Display(Name = "Nivel de autorización 4")]
        AutNivel4,
        [Display(Name = "Contralor")]
        AutContralor
    };
}