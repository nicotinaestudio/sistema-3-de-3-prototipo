﻿/**
* Sistema 3 de 3 para Gobierno del Estado de Jalisco
*
* Desarrollado por Nicotina Estudio
* http://www.nicotinaestudio.com - hola@nicotinaestudio.mx
*
* Creado por: Carlos Isaac Hernández Morfín.
* Fecha de creación: 18/01/2017
**/

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sistema3de3.Models
{
    [Table("ServidoresPublicos")]
    public class ServidorPublico: Usuario
    {
        [Required()]
        [DataType(DataType.Text)]
        [StringLength(50)]
        [Display(Name = "RFC")]
        public string RFC { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha de inicio de la obligación")]
        public DateTime FechaDeInicioDeLaObligacion { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha de terminación de la obligación")]
        public DateTime FechaDeTerminacionDeLaObligacion { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Fecha de último movimiento")]
        public DateTime FechaDeUltimoMovimiento { get; set; }

        public virtual ICollection<Declaracion> Declaraciones { get; set; }
    }
}