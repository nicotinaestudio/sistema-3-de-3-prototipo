namespace Sistema3de3.Migrations
{
    using DAL;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configurationx
    {
        private S3Context context;

        protected void Seed()
        {
            context.Entidades.AddOrUpdate(
              p => p.Nombre,
              new Entidad { Nombre = "Entidad 1" },
              new Entidad { Nombre = "Entidad 2" },
              new Entidad { Nombre = "Entidad 3" }
            );

            context.UnidadesResponsables.AddOrUpdate(
              p => p.Nombre,
              new UnidadResponsable { EntidadId = 1, Nombre = "Unidad 1" },
              new UnidadResponsable { EntidadId = 1, Nombre = "Unidad 2" },
              new UnidadResponsable { EntidadId = 1, Nombre = "Unidad 3" }
            );

            context.Cargos.AddOrUpdate(
              p => p.Nombre,
              new Cargo { EntidadId = 1, Nombre = "Cargo 1" },
              new Cargo { EntidadId = 1, Nombre = "Cargo 2" },
              new Cargo { EntidadId = 1, Nombre = "Cargo 3" }
            );
        }
    }
}
