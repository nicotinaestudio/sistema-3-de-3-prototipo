﻿/**
* Sistema 3 de 3 para Gobierno del Estado de Jalisco
*
* Desarrollado por Nicotina Estudio
* http://www.nicotinaestudio.com - hola@nicotinaestudio.mx
*
* Creado por: Carlos Isaac Hernández Morfín.
* Fecha de creación: 18/01/2017
**/


using Sistema3de3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Sistema3de3.DAL
{
    internal class S3Context : DbContext
    {
        public DbSet<Cargo> Cargos { get; set; }
        public DbSet<Declaracion> Declaraciones { get; set; }
        public DbSet<Entidad> Entidades { get; set; }
        public DbSet<ServidorPublico> ServidoresPublicos { get; set; }
        public DbSet<UnidadResponsable> UnidadesResponsables { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}