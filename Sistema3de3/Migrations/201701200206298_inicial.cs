namespace Sistema3de3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cargos",
                c => new
                    {
                        CargoId = c.Int(nullable: false, identity: true),
                        EsActivo = c.Boolean(nullable: false),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        EntidadId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CargoId)
                .ForeignKey("dbo.Entidades", t => t.EntidadId)
                .Index(t => t.EntidadId);
            
            CreateTable(
                "dbo.Entidades",
                c => new
                    {
                        EntidadId = c.Int(nullable: false, identity: true),
                        EsActivo = c.Boolean(nullable: false),
                        Nombre = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.EntidadId);
            
            CreateTable(
                "dbo.UnidadesResponsables",
                c => new
                    {
                        UnidadResponsableId = c.Int(nullable: false, identity: true),
                        EsActivo = c.Boolean(nullable: false),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        EntidadId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UnidadResponsableId)
                .ForeignKey("dbo.Entidades", t => t.EntidadId)
                .Index(t => t.EntidadId);
            
            CreateTable(
                "dbo.Declaraciones",
                c => new
                    {
                        DeclaracionId = c.Int(nullable: false, identity: true),
                        EsActivo = c.Boolean(nullable: false),
                        EsEnviado = c.Boolean(nullable: false),
                        EsEliminado = c.Boolean(nullable: false),
                        FechaDeRegistro = c.DateTime(nullable: false),
                        DeclaracionConcepto = c.Int(nullable: false),
                        DeclaracionTipo = c.Int(nullable: false),
                        AcuseUID = c.String(),
                        Campo1 = c.String(),
                        Campo2 = c.String(),
                        Campo3 = c.String(),
                        Campo4 = c.String(),
                        Campo5 = c.String(),
                        ServidorPublico_UsuarioId = c.Int(),
                    })
                .PrimaryKey(t => t.DeclaracionId)
                .ForeignKey("dbo.ServidoresPublicos", t => t.ServidorPublico_UsuarioId)
                .Index(t => t.ServidorPublico_UsuarioId);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        UsuarioId = c.Int(nullable: false, identity: true),
                        EsActivo = c.Boolean(nullable: false),
                        EsEliminado = c.Boolean(nullable: false),
                        FechaDeRegistro = c.DateTime(nullable: false),
                        NivelUsuario = c.Int(nullable: false),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        PrimerApellido = c.String(nullable: false, maxLength: 50),
                        SegundoApellido = c.String(nullable: false, maxLength: 50),
                        EntidadId = c.Int(nullable: false),
                        UnidadResponsableId = c.Int(nullable: false),
                        CargoId = c.Int(nullable: false),
                        Telefono = c.String(maxLength: 15),
                        Extension = c.String(),
                        CorreoElectronico = c.String(nullable: false, maxLength: 100),
                        Calle = c.String(),
                        Colonia = c.String(),
                        Numero = c.String(),
                        Estado = c.String(),
                        Municipio = c.String(),
                        CP = c.String(),
                        NumExterior = c.String(),
                        NumInterior = c.String(),
                        FechaDeNacimiento = c.DateTime(nullable: false),
                        EstadoCivil = c.Int(nullable: false),
                        User = c.String(nullable: false, maxLength: 100),
                        Contrasena = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.UsuarioId)
                .ForeignKey("dbo.Cargos", t => t.CargoId)
                .ForeignKey("dbo.Entidades", t => t.EntidadId)
                .ForeignKey("dbo.UnidadesResponsables", t => t.UnidadResponsableId)
                .Index(t => t.EntidadId)
                .Index(t => t.UnidadResponsableId)
                .Index(t => t.CargoId);
            
            CreateTable(
                "dbo.ServidoresPublicos",
                c => new
                    {
                        UsuarioId = c.Int(nullable: false),
                        FechaDeInicioDeLaObligacion = c.DateTime(nullable: false),
                        FechaDeUltimoMovimiento = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.UsuarioId)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId)
                .Index(t => t.UsuarioId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServidoresPublicos", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.Usuarios", "UnidadResponsableId", "dbo.UnidadesResponsables");
            DropForeignKey("dbo.Usuarios", "EntidadId", "dbo.Entidades");
            DropForeignKey("dbo.Usuarios", "CargoId", "dbo.Cargos");
            DropForeignKey("dbo.Declaraciones", "ServidorPublico_UsuarioId", "dbo.ServidoresPublicos");
            DropForeignKey("dbo.UnidadesResponsables", "EntidadId", "dbo.Entidades");
            DropForeignKey("dbo.Cargos", "EntidadId", "dbo.Entidades");
            DropIndex("dbo.ServidoresPublicos", new[] { "UsuarioId" });
            DropIndex("dbo.Usuarios", new[] { "CargoId" });
            DropIndex("dbo.Usuarios", new[] { "UnidadResponsableId" });
            DropIndex("dbo.Usuarios", new[] { "EntidadId" });
            DropIndex("dbo.Declaraciones", new[] { "ServidorPublico_UsuarioId" });
            DropIndex("dbo.UnidadesResponsables", new[] { "EntidadId" });
            DropIndex("dbo.Cargos", new[] { "EntidadId" });
            DropTable("dbo.ServidoresPublicos");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Declaraciones");
            DropTable("dbo.UnidadesResponsables");
            DropTable("dbo.Entidades");
            DropTable("dbo.Cargos");
        }
    }
}
