namespace Sistema3de3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicial2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Declaraciones", "ServidorPublicoId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Declaraciones", "ServidorPublicoId");
        }
    }
}
