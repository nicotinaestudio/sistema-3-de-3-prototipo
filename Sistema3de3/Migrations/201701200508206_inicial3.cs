namespace Sistema3de3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicial3 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Declaraciones", new[] { "ServidorPublico_UsuarioId" });
            RenameColumn(table: "dbo.Declaraciones", name: "ServidorPublico_UsuarioId", newName: "UsuarioId");
            AlterColumn("dbo.Declaraciones", "UsuarioId", c => c.Int(nullable: false));
            CreateIndex("dbo.Declaraciones", "UsuarioId");
            DropColumn("dbo.Declaraciones", "ServidorPublicoId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Declaraciones", "ServidorPublicoId", c => c.Int(nullable: false));
            DropIndex("dbo.Declaraciones", new[] { "UsuarioId" });
            AlterColumn("dbo.Declaraciones", "UsuarioId", c => c.Int());
            RenameColumn(table: "dbo.Declaraciones", name: "UsuarioId", newName: "ServidorPublico_UsuarioId");
            CreateIndex("dbo.Declaraciones", "ServidorPublico_UsuarioId");
        }
    }
}
