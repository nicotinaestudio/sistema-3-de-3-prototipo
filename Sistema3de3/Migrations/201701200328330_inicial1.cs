namespace Sistema3de3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicial1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ServidoresPublicos", "RFC", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.ServidoresPublicos", "FechaDeTerminacionDeLaObligacion", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ServidoresPublicos", "FechaDeTerminacionDeLaObligacion");
            DropColumn("dbo.ServidoresPublicos", "RFC");
        }
    }
}
